FROM hello-world

FROM busybox
COPY --from=0 /hello /usr/local/sbin/
